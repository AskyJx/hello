package com.springsample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {
    @RequestMapping("/hello")

    public ModelAndView hello(){

              ModelAndView mv =new ModelAndView();

              mv.addObject("spring---1", "spring mvc---1");

              mv.setViewName("hello");

              return mv;

    }

}
